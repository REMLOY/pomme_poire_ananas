import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return MaterialApp(
      title: 'Liste',
      home: const ListeApp(
        title: 'Liste',
      ),
    );
  }
}

class ListeApp extends StatefulWidget {
  const ListeApp({super.key, required this.title});

  final String title;

  @override
  State<StatefulWidget> createState() => _ListeAppState();
}

class _ListeAppState extends State<ListeApp> {
  int _counter = 0;
  List<int> listItems = [];

  void _increment() {
    setState(() {
      _counter++;
      listItems.add(_counter);
    });
  }

  bool isPrime(int number) {
    if (number <= 1) return false; // Prime number is > 1
    for (int i = 2; i < number; i++) {
      if (number % i == 0) return false;
    }
    return true;
  }

  Image _getimage(int index) {
    if (isPrime(index)) {
      return const Image(image: AssetImage('images/ananas.png'));
    } else if ((index % 2) == 0) {
      return const Image(image: AssetImage('images/poire.png'));
    } else {
      return const Image(image: AssetImage('images/pomme.png'));
    }
  }

  Color _getcolor(int val) {
    if (val % 2 == 0) {
      return Colors.black;
    } else if (val % 2 != 0) {
      return Colors.green;
    } else if (val == 0) {
      return Colors.blue;
    } else {
      return Colors.blue;
    }
  }

  Text _gettitle(int val) {
    if (isPrime(val)) {
      return const Text(' Premier ');
    } else if ((val % 2) == 0) {
      return const Text(' Pair ');
    } else {
      return const Text(' Impair ');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Row(
            children: [
              Text('$_counter'),
              _gettitle(_counter)
            ],
          ),
        ),
        body: ListView.builder(
          itemCount: listItems.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: _getimage(index),
              title: Text(listItems[index].toString()),
              textColor: Colors.white,
              tileColor: (index % 2 == 0) ? Colors.black : Colors.green,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: _increment, backgroundColor: _getcolor(_counter)),
      ),
    );
  }
}
